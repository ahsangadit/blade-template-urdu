.<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="{{ asset("build/images/favicon.ico")}}" type="image/ico"/>
    <title>@yield('title') </title>
    @yield('css-content')
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>
                <div class="clearfix"></div>

                @include('admin_layout.admin_profile_sidebar_info')

                <br />

                @include('admin_layout.admin_sidebar')

                @include('admin_layout.admin_menu_footer_button')

                </div>            
        </div>


        @include('admin_layout.admin_header')

        @yield('content')

        @include('admin_layout.admin_footer')
      
        </div>
</div>


@yield('js-content')

</body>
</html>

